var express = require('express');
var router = express.Router();
var axios = require('axios');

const access_token = "EAAE6U5ZAcyUEBAHWgTZAbT07GZBYNPpUUlQMlm6BKJ4HTj8PG6wfrZAIt3h0sqm3Y0A94TEIDTeiFV50zZB5rSZCLUu201qnutXRoiTyEtQlqWHeusmSPi8BWupHQSuuPcSpj29q0nqdLdK4k2S2zSyFSiyKiCeZAABYZC0LIdH2CQZDZD";

/* GET home page. */
router.get('/videos', function(req, res, next) {
    const videos = [

        {
            id: 1,
            thumbnail: "https://img.youtube.com/vi/icvNbcT2fGc/0.jpg",
            name: "Eurovision'16 / 360°/ Virtual reality",
            url: "https://www.youtube.com/watch?v=icvNbcT2fGc",
            is360Deg: true,
            lang: "english"
        },
        {
            id: 2,
            thumbnail: "https://img.youtube.com/vi/jV1sjm9Lz_Q/0.jpg",
            name: "France Gall - Poupee De Cire, Poupee De Son (1965)",
            url: "https://www.youtube.com/watch?v=jV1sjm9Lz_Q",
            is360Deg: false,
            lang: "hebrew"
        },
        {
            id: 3,
            thumbnail: "https://img.youtube.com/vi/8fvLtTRzdHw/0.jpg",
            name: "Winner - Conchita Wurst - Rise Like A Phoenix - Austria - Live at the 2014 Eurovision Song Contest",
            url: "https://www.youtube.com/watch?v=8fvLtTRzdHw",
            is360Deg: false,
            lang: "german"
        },
        {
            id: 4,
            thumbnail: "https://img.youtube.com/vi/uzmnLARim9U/0.jpg",
            name: "360 Viana do Castelo – Madame Monsieur’s Postcard Eurovision 2018",
            url: "https://www.youtube.com/watch?v=uzmnLARim9U",
            is360Deg: true,
            lang: "german"
        },
        {
            id: 5,
            thumbnail: "https://img.youtube.com/vi/Xu8pR5ssuoo/0.jpg",
            name: "Joci Pápai - Origo (Hungary) 360° A dal performance",
            url: "https://www.youtube.com/watch?v=Xu8pR5ssuoo",
            is360Deg: true,
            lang: "hungarian"
        },
        {
            id: 6,
            thumbnail: "https://img.youtube.com/vi/0DZ7DbdeOeE/0.jpg",
            name: "Anggun - Echo (You And I) - Live - Grand Final - 2012 Eurovision Song Contest",
            url: "https://www.youtube.com/watch?v=0DZ7DbdeOeE",
            is360Deg: false,
            lang: "french"
        },
        {
            id: 7,
            thumbnail: "https://img.youtube.com/vi/VlpBPO9_L4E/0.jpg",
            name: "Kate Miller-Heidke - Zero Gravity - Australia 🇦🇺 - Official Video - Eurovision 2019",
            url: "https://www.youtube.com/watch?v=VlpBPO9_L4E",
            is360Deg: false,
            lang: "english"
        },
        {
            id: 8,
            thumbnail: "https://img.youtube.com/vi/ymFVfzu-2mw/0.jpg",
            name: "Salvador Sobral - Amar Pelos Dois (Portugal) Eurovision 2017 - Official Music Video",
            url: "https://www.youtube.com/watch?v=ymFVfzu-2mw",
            is360Deg: false,
            lang: "portuguese"
        },
        {
            id: 9,
            thumbnail: "https://img.youtube.com/vi/VCG2rw4ZXTY/0.jpg",
            name: "Ukraine: \"1944\" by Jamala - Winner of Eurovision Song Contest 2016 - BBC",
            url: "https://www.youtube.com/watch?v=VCG2rw4ZXTY",
            is360Deg: false,
            lang: "russian"
        }

    ];

    res.send(videos);

});

router.get('/videos_broadcast', function(req, res, next) {


    const url = `https://graph.facebook.com/v3.2/651877778577111?fields=video_broadcasts&access_token=${access_token}`;
    const facebookapi = `https://graph.facebook.com/v3.2`;
    let data=null;


    axios.get(url)
        .then(function(response){
           // console.log('get111 ',response.data.video_broadcasts);
            const videos = response.data.video_broadcasts.data;
            let videoPromises = [];
            let videoIds = [];

           // videoPromises.push(axios.get("https://graph.facebook.com/v3.2/654946144970303?fields=id,description,embed_html,permalink_url,title, content_tags&access_token=EAAE6U5ZAcyUEBAMOnUF6zoKFXWbte4asItJ6TuYs7kBjgy0hzuyAZCW6hnVyGJVVT3JZC2rDjTcmuZBzz4zpjXlEMZCzcw36ZAnWZALuubbZAA1bS0ytVJRxRADo29eBJypFcnPvZCgf4GRMB1qpmzUf7kNCAitGKj3L1R4st0GT5pQx74bfO0w9jNGR1HhGXvO8ZD"));
           //  videoPromises.push(axios.get(`${facebookapi}/651925318572357?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/651924545239101?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/651893721908850?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/654946144970303?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/651893708575518?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/651890101909212?fields=id&access_token=${access_token}`));
           //  videoPromises.push(axios.get(`${facebookapi}/651890045242551?fields=id&access_token=${access_token}`));

            videos.forEach(function(video){
                let promise = axios.get(`${facebookapi}/${video.id}?fields=id,description,embed_html,permalink_url,title,status&access_token=${access_token}`);
                videoPromises.push(promise);
                videoIds.push(video.id);
               // console.log('ids ',videoIds);
            });

            //console.log('promises are ',videoPromises);
            Promise.all(videoPromises)
                .then(function(response){
                   // console.log('test ',response[0]);
                    let returnData = response.map(function(element){return {
                        "id": element.data.id,
                        "status": element.data.status,
                        "title": element.data.title ? element.data.title : '',
                        "description": element.data.description ? element.data.description : '',
                        "embed_html": element.data.embed_html,
                        "permalink_url": element.data.permalink_url

                    }

                    });
                    console.log('success all promises!!! ', returnData);
                    //res.send(JSON.stringify(response));
                    res.send({
                        data: returnData
                    });
                    //console.log('got all promises ',response[0].data);
                 //   let data = response.map(function(videopromise){return "test"});
                    //res.send("test");
                })
                .catch(function(error){
                    console.log('all promisses error is ');
                });
            // axios.all(videoPromises)
            //     .then(function(response){
            //         console.log('video promises ',response);
            //         res.send(response);
            //
            //     })
            //     .catch();

           // res.send(videoIds);

        })
        .catch(function(error){
            //console.log('error ',error);
        });





});

module.exports = router;
